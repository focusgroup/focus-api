import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Room {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ length: 80 })
  name: string
}
