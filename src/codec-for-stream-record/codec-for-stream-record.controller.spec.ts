import { Test, TestingModule } from '@nestjs/testing'
import { CodecForStreamRecordController } from './codec-for-stream-record.controller'
import { CodecForStreamRecordService } from './codec-for-stream-record.service'

describe('CodecForStreamRecordController', () => {
  let controller: CodecForStreamRecordController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CodecForStreamRecordController],
      providers: [CodecForStreamRecordService],
    }).compile()

    controller = module.get<CodecForStreamRecordController>(
      CodecForStreamRecordController,
    )
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})
