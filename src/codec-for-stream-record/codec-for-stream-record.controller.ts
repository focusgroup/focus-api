import { Controller, Get, Query } from '@nestjs/common'
import { CodecForStreamRecordService } from './codec-for-stream-record.service'
import { ApiTags } from '@nestjs/swagger'

@ApiTags('codec-for-stream-record')
@Controller('codec-for-stream-record')
export class CodecForStreamRecordController {
  constructor(
    private readonly codecForStreamRecordService: CodecForStreamRecordService,
  ) {}

  @Get()
  getCodecForStreamRecord(@Query('streamRecordId') streamRecordId: string) {
    return this.codecForStreamRecordService.getCodecForStreamRecord(
      streamRecordId,
    )
  }
}
