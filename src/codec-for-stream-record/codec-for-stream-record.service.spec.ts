import { Test, TestingModule } from '@nestjs/testing'
import { CodecForStreamRecordService } from './codec-for-stream-record.service'

describe('CodecForStreamRecordService', () => {
  let service: CodecForStreamRecordService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CodecForStreamRecordService],
    }).compile()

    service = module.get<CodecForStreamRecordService>(
      CodecForStreamRecordService,
    )
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
