import { Injectable } from '@nestjs/common'

@Injectable()
export class CodecForStreamRecordService {
  getCodecForStreamRecord(streamRecordId: string) {
    return `This action returns the best codec id for the #${streamRecordId} streamRecord`
  }
}
