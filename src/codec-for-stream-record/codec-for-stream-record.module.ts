import { Module } from '@nestjs/common'
import { CodecForStreamRecordService } from './codec-for-stream-record.service'
import { CodecForStreamRecordController } from './codec-for-stream-record.controller'

@Module({
  controllers: [CodecForStreamRecordController],
  providers: [CodecForStreamRecordService],
})
export class CodecForStreamRecordModule {}
