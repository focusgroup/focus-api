import { Module } from '@nestjs/common'
import { RoomsModule } from './rooms/rooms.module'
import { UsersModule } from './users/users.module'
import { CodecsModule } from './codecs/codecs.module'
import { StreamRecordsModule } from './stream-records/stream-records.module'
import { CodecForStreamRecordModule } from './codec-for-stream-record/codec-for-stream-record.module'

@Module({
  imports: [
    RoomsModule,
    UsersModule,
    CodecsModule,
    StreamRecordsModule,
    CodecForStreamRecordModule,
  ],
})
export class AppModule {}
