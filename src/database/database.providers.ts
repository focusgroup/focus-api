import { createConnection } from 'typeorm'
import * as path from 'path'
import { databaseConnection } from '../constants'

export const databaseProviders = [
  {
    provide: databaseConnection,
    useFactory: async () =>
      await createConnection({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'root',
        database: 'test',
        entities: [path.resolve(__dirname, '/../**/*.entity{.ts,.js}')],
        synchronize: true,
      }),
  },
]
