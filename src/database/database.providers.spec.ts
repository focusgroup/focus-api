import { Test, TestingModule } from '@nestjs/testing'
import { databaseProviders } from './database.providers'
import { Connection } from 'typeorm'

describe('Database providers', () => {
  let providers: { provide: string; useFactory: () => Promise<Connection> }[]
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [...databaseProviders],
    }).compile()

    providers = module.get('providers')
  })

  it('should be defined', () => {
    expect(providers).toBeDefined()
  })
})
