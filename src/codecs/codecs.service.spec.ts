import { Test, TestingModule } from '@nestjs/testing'
import { CodecsService } from './codecs.service'

describe('CodecsService', () => {
  let service: CodecsService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CodecsService],
    }).compile()

    service = module.get<CodecsService>(CodecsService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
