import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common'
import { CodecsService } from './codecs.service'
import { CreateCodecDto } from './dto/create-codec.dto'
import { UpdateCodecDto } from './dto/update-codec.dto'
import { ApiTags } from '@nestjs/swagger'

@ApiTags('codecs')
@Controller('codecs')
export class CodecsController {
  constructor(private readonly codecsService: CodecsService) {}

  @Post()
  create(@Body() createCodecDto: CreateCodecDto) {
    return this.codecsService.create(createCodecDto)
  }

  @Get()
  findAll() {
    return this.codecsService.findAll()
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.codecsService.findOne(+id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCodecDto: UpdateCodecDto) {
    return this.codecsService.update(+id, updateCodecDto)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.codecsService.remove(+id)
  }
}
