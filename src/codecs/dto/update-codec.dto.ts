import { PartialType } from '@nestjs/swagger'
import { CreateCodecDto } from './create-codec.dto'

export class UpdateCodecDto extends PartialType(CreateCodecDto) {}
