import { Module } from '@nestjs/common'
import { CodecsService } from './codecs.service'
import { CodecsController } from './codecs.controller'

@Module({
  controllers: [CodecsController],
  providers: [CodecsService],
})
export class CodecsModule {}
