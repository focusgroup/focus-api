import { Test, TestingModule } from '@nestjs/testing'
import { CodecsController } from './codecs.controller'
import { CodecsService } from './codecs.service'

describe('CodecsController', () => {
  let controller: CodecsController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CodecsController],
      providers: [CodecsService],
    }).compile()

    controller = module.get<CodecsController>(CodecsController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})
