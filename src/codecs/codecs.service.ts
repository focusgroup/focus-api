import { Injectable } from '@nestjs/common'
import { CreateCodecDto } from './dto/create-codec.dto'
import { UpdateCodecDto } from './dto/update-codec.dto'

@Injectable()
export class CodecsService {
  create(createCodecDto: CreateCodecDto) {
    return 'This action adds a new codec'
  }

  findAll() {
    return `This action returns all codecs`
  }

  findOne(id: number) {
    return `This action returns a #${id} codec`
  }

  update(id: number, updateCodecDto: UpdateCodecDto) {
    return `This action updates a #${id} codec`
  }

  remove(id: number) {
    return `This action removes a #${id} codec`
  }
}
