import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common'
import { StreamRecordsService } from './stream-records.service'
import { CreateStreamRecordDto } from './dto/create-stream-record.dto'
import { UpdateStreamRecordDto } from './dto/update-stream-record.dto'
import { ApiTags } from '@nestjs/swagger'

@ApiTags('stream-records')
@Controller('stream-records')
export class StreamRecordsController {
  constructor(private readonly streamRecordsService: StreamRecordsService) {}

  @Post()
  create(@Body() createStreamRecordDto: CreateStreamRecordDto) {
    return this.streamRecordsService.create(createStreamRecordDto)
  }

  @Get()
  findAll() {
    return this.streamRecordsService.findAll()
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.streamRecordsService.findOne(+id)
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateStreamRecordDto: UpdateStreamRecordDto,
  ) {
    return this.streamRecordsService.update(+id, updateStreamRecordDto)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.streamRecordsService.remove(+id)
  }
}
