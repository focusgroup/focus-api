import { Module } from '@nestjs/common'
import { StreamRecordsService } from './stream-records.service'
import { StreamRecordsController } from './stream-records.controller'

@Module({
  controllers: [StreamRecordsController],
  providers: [StreamRecordsService],
})
export class StreamRecordsModule {}
