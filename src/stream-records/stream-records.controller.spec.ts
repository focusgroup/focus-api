import { Test, TestingModule } from '@nestjs/testing'
import { StreamRecordsController } from './stream-records.controller'
import { StreamRecordsService } from './stream-records.service'

describe('StreamRecordsController', () => {
  let controller: StreamRecordsController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StreamRecordsController],
      providers: [StreamRecordsService],
    }).compile()

    controller = module.get<StreamRecordsController>(StreamRecordsController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})
