import { Injectable } from '@nestjs/common'
import { CreateStreamRecordDto } from './dto/create-stream-record.dto'
import { UpdateStreamRecordDto } from './dto/update-stream-record.dto'

@Injectable()
export class StreamRecordsService {
  create(createStreamRecordDto: CreateStreamRecordDto) {
    return 'This action adds a new streamRecord'
  }

  findAll() {
    return `This action returns all streamRecords`
  }

  findOne(id: number) {
    return `This action returns a #${id} streamRecord`
  }

  update(id: number, updateStreamRecordDto: UpdateStreamRecordDto) {
    return `This action updates a #${id} streamRecord`
  }

  remove(id: number) {
    return `This action removes a #${id} streamRecord`
  }
}
