import { PartialType } from '@nestjs/swagger'
import { CreateStreamRecordDto } from './create-stream-record.dto'

export class UpdateStreamRecordDto extends PartialType(CreateStreamRecordDto) {}
