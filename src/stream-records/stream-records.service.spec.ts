import { Test, TestingModule } from '@nestjs/testing'
import { StreamRecordsService } from './stream-records.service'

describe('StreamRecordsService', () => {
  let service: StreamRecordsService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StreamRecordsService],
    }).compile()

    service = module.get<StreamRecordsService>(StreamRecordsService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
